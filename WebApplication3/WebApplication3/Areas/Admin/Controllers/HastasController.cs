﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Areas.Admin.Controllers
{
    [Authorize]
    public class HastasController : Controller
    {
        private Model1 db = new Model1();

        // GET: Admin/Hastas
        public ActionResult Index()
        {
            var hasta = db.Hasta.Include(h => h.Birim);
            return View(hasta.ToList());
        }

        // GET: Admin/Hastas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hasta hasta = db.Hasta.Find(id);
            if (hasta == null)
            {
                return HttpNotFound();
            }
            return View(hasta);
        }

        // GET: Admin/Hastas/Create
        public ActionResult Create()
        {
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd");
            return View();
        }

        // POST: Admin/Hastas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HastaId,HastaAd,HastaSoyad,HastaTC,BirimID")] Hasta hasta)
        {
            if (ModelState.IsValid)
            {
                db.Hasta.Add(hasta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", hasta.BirimID);
            return View(hasta);
        }

        // GET: Admin/Hastas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hasta hasta = db.Hasta.Find(id);
            if (hasta == null)
            {
                return HttpNotFound();
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", hasta.BirimID);
            return View(hasta);
        }

        // POST: Admin/Hastas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HastaId,HastaAd,HastaSoyad,HastaTC,BirimID")] Hasta hasta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hasta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", hasta.BirimID);
            return View(hasta);
        }

        // GET: Admin/Hastas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hasta hasta = db.Hasta.Find(id);
            if (hasta == null)
            {
                return HttpNotFound();
            }
            return View(hasta);
        }

        // POST: Admin/Hastas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hasta hasta = db.Hasta.Find(id);
            db.Hasta.Remove(hasta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
