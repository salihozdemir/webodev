﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    [Authorize]
    public class DoktorController : Controller
    {
        private Model1 db = new Model1();

        // GET: Doktor
        public ActionResult Index()
        {
            var doktor = db.Doktor.Include(d => d.Birim).Include(d => d.Unvan);
            return View(doktor.ToList());
        }

        // GET: Doktor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doktor doktor = db.Doktor.Find(id);
            if (doktor == null)
            {
                return HttpNotFound();
            }
            return View(doktor);
        }

        // GET: Doktor/Create
        public ActionResult Create()
        {
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd");
            ViewBag.UnvanID = new SelectList(db.Unvan, "UnvanId", "UnvanAdi");
            return View();
        }

        // POST: Doktor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DoktorId,DoktorAd,DoktorSoyad,BirimID,UnvanID")] Doktor doktor)
        {
            if (ModelState.IsValid)
            {
                db.Doktor.Add(doktor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", doktor.BirimID);
            ViewBag.UnvanID = new SelectList(db.Unvan, "UnvanId", "UnvanAdi", doktor.UnvanID);
            return View(doktor);
        }

        // GET: Doktor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doktor doktor = db.Doktor.Find(id);
            if (doktor == null)
            {
                return HttpNotFound();
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", doktor.BirimID);
            ViewBag.UnvanID = new SelectList(db.Unvan, "UnvanId", "UnvanAdi", doktor.UnvanID);
            return View(doktor);
        }

        // POST: Doktor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DoktorId,DoktorAd,DoktorSoyad,BirimID,UnvanID")] Doktor doktor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(doktor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BirimID = new SelectList(db.Birim, "BirimId", "BirimAd", doktor.BirimID);
            ViewBag.UnvanID = new SelectList(db.Unvan, "UnvanId", "UnvanAdi", doktor.UnvanID);
            return View(doktor);
        }

        // GET: Doktor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doktor doktor = db.Doktor.Find(id);
            if (doktor == null)
            {
                return HttpNotFound();
            }
            return View(doktor);
        }

        // POST: Doktor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Doktor doktor = db.Doktor.Find(id);
            db.Doktor.Remove(doktor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
