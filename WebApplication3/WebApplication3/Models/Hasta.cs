namespace WebApplication3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hasta")]
    public partial class Hasta
    {
        public int HastaId { get; set; }

        [Required]
        [StringLength(50)]
        public string HastaAd { get; set; }

        [Required]
        [StringLength(50)]
        public string HastaSoyad { get; set; }

        public long HastaTC { get; set; }

        public int BirimID { get; set; }

        public virtual Birim Birim { get; set; }
    }
}
