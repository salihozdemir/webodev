namespace WebApplication3
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Birim> Birim { get; set; }
        public virtual DbSet<Doktor> Doktor { get; set; }
        public virtual DbSet<Hasta> Hasta { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Unvan> Unvan { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Birim>()
                .HasMany(e => e.Doktor)
                .WithRequired(e => e.Birim)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Birim>()
                .HasMany(e => e.Hasta)
                .WithRequired(e => e.Birim)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Unvan>()
                .HasMany(e => e.Doktor)
                .WithRequired(e => e.Unvan)
                .WillCascadeOnDelete(false);
        }
    }
}
