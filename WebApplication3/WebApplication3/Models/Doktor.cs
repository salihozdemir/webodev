namespace WebApplication3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Doktor")]
    public partial class Doktor
    {
        public int DoktorId { get; set; }

        [Required]
        [StringLength(50)]
        public string DoktorAd { get; set; }

        [Required]
        [StringLength(50)]
        public string DoktorSoyad { get; set; }

        public int BirimID { get; set; }

        public int UnvanID { get; set; }

        public virtual Birim Birim { get; set; }

        public virtual Unvan Unvan { get; set; }
    }
}
